// Fill out your copyright notice in the Description page of Project Settings.


#include "Projectile.h"
#include "Zombie.h"
#include "PaperSpriteComponent.h"
#include <Runtime\Engine\Classes\Kismet\GameplayStatics.h>

// Sets default values
AProjectile::AProjectile()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!RootComponent)
		RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("ProjectileBase"));
	ProjectileSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("ProjectileSprite"));
	ProjectileSprite->AttachTo(RootComponent);


	Speed = 1000;
	Damage = 25;
}

// Called when the game starts or when spawned
void AProjectile::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void AProjectile::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	FVector Loc = GetActorLocation();
	Loc += (DeltaTime * Speed) * GetTransform().GetUnitAxis(EAxis::X);
	SetActorLocation(Loc);

	UWorld* World = GetWorld();
	if (World) {
		TArray<AActor*> Zombies;
		UGameplayStatics::GetAllActorsOfClass(World, AZombie::StaticClass(), Zombies);
		for (int i = 0; i < Zombies.Num(); i++) {
			FVector ZLoc = Zombies[i]->GetActorLocation();
			FVector2D RelativeLocationOfBullet(ZLoc.X - Loc.X, ZLoc.Y - Loc.Y);
			AZombie* Zombie = Cast<AZombie>(Zombies[i]);
			if (Zombie) {
				float BoundingBoxMax = 5+FMath::Max<float>(Zombie->BoundingBoxWidth, Zombie->BoundingBoxHeight);
				if (FMath::Abs(RelativeLocationOfBullet.X) < BoundingBoxMax && FMath::Abs(RelativeLocationOfBullet.Y) < BoundingBoxMax) {
					FRotator ZRot = Zombie->ZombieDirection->GetRelativeRotation();
					FVector ZRotVector = ZRot.Vector();
					FVector2D ZRotV(ZRotVector.X, ZRotVector.Y);
					FVector2D RLoBNormal = RelativeLocationOfBullet.GetSafeNormal();
					float Angle = FMath::Acos(FVector2D::DotProduct(RLoBNormal, ZRotV));
					FVector2D RotatedProjectileLoc(
						RelativeLocationOfBullet.X * FMath::Cos(Angle) - RelativeLocationOfBullet.Y * FMath::Sin(Angle),
						RelativeLocationOfBullet.X * FMath::Sin(Angle) + RelativeLocationOfBullet.Y * FMath::Cos(Angle)
					);

					if (FMath::Abs(RotatedProjectileLoc.X) < Zombie->BoundingBoxWidth && FMath::Abs(RotatedProjectileLoc.Y) < Zombie->BoundingBoxHeight) {
						Zombie->HitByProjectile(this);
						Destroy();
					}
				}
			}
		}
	}
}
