// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "GameFramework/Actor.h"
#include "WeaponA.generated.h"

UCLASS()
class ZOMBIEDEFENSE_API AWeaponA : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeaponA();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Weapon Damage
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0.0"))
		float Damage = 30;*/
	//Weapon Rate of Fire (shots per minute)
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0.0"))
		float RateOfFire = 120;
	//Weapon Mag size
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0.0"))
		float MagSize = 10;
	//Weapon reload time in seconds
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0.0"))
		float ReloadTime = 1;
	//Weapon range
	/*UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Weapon", meta = (ClampMin = "0.0"))
		float Range = 500;*/
	// Projectile to spawn when firing.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Weapon")
		TSubclassOf<AActor> Projectile;
private:
	//Bullets in mag
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (ClampMin = "0.0", AllowPrivateAccess = "true"))
		float Bullets;
	//Is busy reloading
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		bool IsReloading = false;


	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
		class APlayerPawn* PlayerPawn;



	virtual void Shoot();

	virtual void ReloadFinished();

	//Handle to manage the reload timer
	FTimerHandle ReloadTimerHandle;

	//Time of last shot
	float LastShotTime = 0;

	UAudioComponent* GunshotAudioComponent;
	UAudioComponent* GunEmptyAudioComponent;
	UAudioComponent* GunReloadAudioComponent;
public:	
	//Sprite for the weapon
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
	class UPaperSpriteComponent* WeaponSprite;
	//Sound to play when the gun shoots
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
	USoundCue* GunshotSoundCue;
	//Sound to play when the gun clicks empty
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
	USoundCue* GunEmptySoundCue;
	//Sound to play when the gun reload is finished
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly, Category = "Weapon", meta = (AllowPrivateAccess = "true"))
	USoundCue* GunReloadSoundCue;
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Reload();
};
