// Fill out your copyright notice in the Description page of Project Settings.


#include "Weapon.h"
#include "PlayerPawn.h"

// Sets default values for this component's properties
UWeapon::UWeapon()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UWeapon::BeginPlay()
{
	Super::BeginPlay();

	// ...
	
}


// Called every frame
void UWeapon::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);


	UE_LOG(LogTemp, Warning, TEXT("Weapon Tick"));
	if (PlayerPawn && PlayerPawn->GetPlayerCharInput().Shooting) {
		UE_LOG(LogTemp, Warning, TEXT("Shoot Tick"));
		if (UWorld* World = GetWorld()) {
			float CurrentTime = World->GetTimeSeconds();
			float TimeBetweenShots = 60/RateOfFire;
			if (LastShotTime + TimeBetweenShots > CurrentTime) {
				LastShotTime = CurrentTime;
				UE_LOG(LogTemp, Warning, TEXT("Shoot Call"));
				Shoot();
			}
		}
	}
}

void UWeapon::Shoot() {
	if (Bullets > 0) {
		Bullets--;
		UE_LOG(LogTemp, Warning, TEXT("Shoot function start"));

		if (UWorld* World = GetWorld())
			if (AActor* NewProjectile = World->SpawnActor(Projectile)) {
				UE_LOG(LogTemp, Warning, TEXT("Projectile created"));
				FVector NPLoc = PlayerPawn->PlayerDirection->GetComponentLocation();
				FRotator NPRot = PlayerPawn->PlayerDirection->GetComponentRotation();
				NewProjectile->SetActorLocation(NPLoc);
				NewProjectile->SetActorRotation(NPRot);
			}
	}
}

void UWeapon::Reload() {
	if (!IsReloading) {
		IsReloading = true;
		if (UWorld* World = GetWorld())
			World->GetTimerManager().SetTimer(ReloadTimerHandle, this, &UWeapon::ReloadFinished, ReloadTime, false);
	}
}

void UWeapon::ReloadFinished() {
	IsReloading = false;
	Bullets = MagSize;
}