// Fill out your copyright notice in the Description page of Project Settings.


#include "WeaponA.h"
#include "PlayerPawn.h"
#include "PaperSpriteComponent.h"

// Sets default values
AWeaponA::AWeaponA()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	WeaponSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("WeaponSprite"));
	WeaponSprite->AttachTo(RootComponent);
	//WeaponSprite->SetMobility(EComponentMobility::Movable);

	GunshotSoundCue = CreateDefaultSubobject<USoundCue>(TEXT("GunshotSoundCue"));
	GunshotAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("GunshotAudioComponent"));
	GunEmptySoundCue = CreateDefaultSubobject<USoundCue>(TEXT("GunEmptySoundCue"));
	GunEmptyAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("GunEmptyAudioComponent"));
	GunReloadSoundCue = CreateDefaultSubobject<USoundCue>(TEXT("GunReloadSoundCue"));
	GunReloadAudioComponent = CreateDefaultSubobject<UAudioComponent>(TEXT("GunReloadAudioComponent"));
}

// Called when the game starts or when spawned
void AWeaponA::BeginPlay()
{
	Super::BeginPlay();
	
	check(GetParentComponent());
	PlayerPawn = Cast<APlayerPawn>(GetParentComponent()->GetOwner());
	check(PlayerPawn);
	PlayerPawn->RegisterWeapon(this);

	Bullets = MagSize;

	if (GunshotSoundCue && GunshotAudioComponent)
		GunshotAudioComponent->SetSound(GunshotSoundCue);
	if (GunEmptySoundCue && GunEmptyAudioComponent)
		GunEmptyAudioComponent->SetSound(GunEmptySoundCue);
	if (GunReloadSoundCue && GunReloadAudioComponent)
		GunReloadAudioComponent->SetSound(GunReloadSoundCue);
}

// Called every frame
void AWeaponA::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (PlayerPawn && PlayerPawn->GetPlayerCharInput().Shooting && !IsReloading) {
		if (UWorld* World = GetWorld()) {
			float CurrentTime = World->GetTimeSeconds();
			float TimeBetweenShots = 60 / RateOfFire;
			if (LastShotTime + TimeBetweenShots <= CurrentTime) {
				LastShotTime = CurrentTime;
				Shoot();
			}
		}
	}
}



void AWeaponA::Shoot() {
	if (Bullets > 0) {
		Bullets--;
		//UE_LOG(LogTemp, Warning, TEXT("Shoot function start"));

		if (UWorld* World = GetWorld())
			if (AActor* NewProjectile = World->SpawnActor(Projectile)) {
				//UE_LOG(LogTemp, Warning, TEXT("Projectile created"));
				//FVector NPLoc = PlayerPawn->PlayerDirection->GetComponentLocation();

				APlayerController* PlayerController = Cast<APlayerController>(PlayerPawn->GetController());
				float MouseX, MouseY;
				FRotator NPRot = PlayerPawn->PlayerDirection->GetComponentRotation();
				if (PlayerController && PlayerController->GetMousePosition(MouseX, MouseY)) {
					FVector2D GunOnScreenPosition;
					UGameplayStatics::ProjectWorldToScreen(PlayerController, GetActorLocation(), GunOnScreenPosition);
					float X = MouseX - GunOnScreenPosition.X;
					float Y = MouseY - GunOnScreenPosition.Y;
					FVector2D MouseDirection(X, Y); MouseDirection = MouseDirection.GetSafeNormal();
					if (!MouseDirection.IsNearlyZero()) {
						float Angle = FMath::RadiansToDegrees(FMath::Atan2(MouseDirection.Y, MouseDirection.X));
						FRotator Rotation = PlayerPawn->PlayerDirection->GetComponentRotation();
						Rotation.Yaw = Angle + 90;
						NewProjectile->SetActorRotation(Rotation);
					}
				}

				FVector NPLoc = GetActorLocation();
				NPLoc += 10 * GetTransform().GetUnitAxis(EAxis::X);
				NewProjectile->SetActorLocation(NPLoc);
				NewProjectile->SetLifeSpan(2);
				//Play shooting sound
				if (GunshotSoundCue && GunshotAudioComponent)
					GunshotAudioComponent->Play();
			}
	}
	else {
		UE_LOG(LogTemp, Warning, TEXT("Empty gun"));
		//Play clicking sound to signal empty gun
		if (GunEmptySoundCue && GunEmptyAudioComponent) {
			GunEmptyAudioComponent->Play();
						UE_LOG(LogTemp, Warning, TEXT("Empty gun sound played"));
		}
	}
}

void AWeaponA::Reload() {
	if (!IsReloading) {
		IsReloading = true;
		if (UWorld* World = GetWorld())
			World->GetTimerManager().SetTimer(ReloadTimerHandle, this, &AWeaponA::ReloadFinished, ReloadTime, false);
	}
}

void AWeaponA::ReloadFinished() {
	IsReloading = false;
	Bullets = MagSize;
	//Play reload sound
	UE_LOG(LogTemp, Warning, TEXT("Reload finished"));
	if (GunReloadSoundCue && GunReloadAudioComponent)
		GunReloadAudioComponent->Play();
}