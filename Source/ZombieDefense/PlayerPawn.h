// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

// This struct covers all possible player input schemes.
USTRUCT(BlueprintType)
struct FPlayerCharInput
{
	GENERATED_BODY()

public:
	// Sanitized movement input, usable for game logic.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerInput")
	FVector2D MovementInput;

	// if the shoot button is held or not.
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerInput")
	bool Shooting = false;;

	void Sanitize();
	void MoveX(float AxisValue);
	void MoveY(float AxisValue);
	void SetShooting(bool bPressed);
private:
	// Code only. Blueprints should not need to know about this. This should not be used for game logic.
	FVector2D RawMovementInput;
};

UCLASS()
class ZOMBIEDEFENSE_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	//Movement Speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "PlayerCharacter", meta = (ClampMin = "0.0"))
	float MovementSpeed = 30;
	//Health Points
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "PlayerCharacter", meta = (ClampMin = "0.0"))
	float HealthPoints = 100;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	//arrow to determine facing
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCharacter", meta = (AllowPrivateAccess = "true"))
	UArrowComponent* PlayerDirection;

	//Sprite for the player body
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCharacter", meta = (AllowPrivateAccess = "true"))
	class UPaperSpriteComponent* PlayerSprite;

	//The Camera
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCharacter", meta = (AllowPrivateAccess = "true"))
	UCameraComponent* CameraComp;

	//The Weapon
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "PlayerCharacter", meta = (AllowPrivateAccess = "true"))
	UChildActorComponent* WeaponB;
	
	void HitByZombie(float Damage);

	class AWeaponA* Weapon;

	void MoveX(float AxisValue);
	void MoveY(float AxisValue);
	void ShootPressed();
	void ShootReleased();
	void ReloadPressed();

	FORCEINLINE const FPlayerCharInput& GetPlayerCharInput() { return PlayerCharInput; }

	FORCEINLINE void RegisterWeapon(AWeaponA* RWeapon) { Weapon = RWeapon; }

	UFUNCTION(BlueprintCallable)
	FORCEINLINE float GetHealthPoints() { return HealthPoints; }
private:
	FPlayerCharInput PlayerCharInput;
};

/*UCLASS(BlueprintType)
class ZOMBIEDEFENSE_API USomeCameraShake : public UCameraShake
{
	GENERATED_BODY()
};*/