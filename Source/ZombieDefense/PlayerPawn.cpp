// Fill out your copyright notice in the Description page of Project Settings.


#include "PlayerPawn.h"
#include "WeaponA.h"
#include "PaperSpriteComponent.h"

// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	if (!RootComponent) {
		RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("PlayerBase"));
		RootComponent->SetMobility(EComponentMobility::Movable);
	}

	PlayerDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("PlayerDirection"));
	PlayerDirection->AttachTo(RootComponent);
	//PlayerDirection->bHiddenInGame = false;
	PlayerDirection->SetMobility(EComponentMobility::Movable);

	PlayerSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("PlayerSprite"));
	PlayerSprite->AttachTo(PlayerDirection);
	PlayerSprite->SetMobility(EComponentMobility::Movable);


	USpringArmComponent* SpringArm = CreateDefaultSubobject<USpringArmComponent>(TEXT("SpringArm"));
	SpringArm->TargetArmLength = 750.0f;
	SpringArm->bEnableCameraLag = true;
	SpringArm->bEnableCameraLag = false;
	SpringArm->bUsePawnControlRotation = false;
	SpringArm->CameraLagSpeed = 2.0f;
	SpringArm->bDoCollisionTest = false;
	SpringArm->AttachTo(RootComponent);
	SpringArm->SetWorldRotation(FRotator(-90.0f, 0.0f, 0.0f));
	SpringArm->SetMobility(EComponentMobility::Movable);

	CameraComp = CreateDefaultSubobject<UCameraComponent>(TEXT("Camera"));
	CameraComp->bUsePawnControlRotation = false;
	CameraComp->ProjectionMode = ECameraProjectionMode::Orthographic;
	CameraComp->OrthoWidth = 1200.0f;
	CameraComp->AspectRatio = 3.0f / 4.0f;
	CameraComp->AttachTo(SpringArm, USpringArmComponent::SocketName);
	CameraComp->SetMobility(EComponentMobility::Movable);

	//Weapon = CreateDefaultSubobject<UChildActorComponent>(TEXT("Weapon"));
	//AddOwnedComponent(Weapon);
	//Weapon->AttachTo(PlayerDirection);
	WeaponB = CreateDefaultSubobject<UChildActorComponent>(TEXT("WeaponB"));
	WeaponB->AttachTo(PlayerDirection);
	//WeaponB->SetMobility(EComponentMobility::Movable);
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
}

// Called every frame
void APlayerPawn::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);


	//UE_LOG(LogTemp, Warning, TEXT("MoveX: %f"), PlayerCharInput.MovementInput.X);
	// Move the character
	{
		PlayerCharInput.Sanitize();
		//FVector MovementDirection = PlayerDirection->GetForwardVector() * sign;
		FVector Pos = GetActorLocation();
		Pos.X += PlayerCharInput.MovementInput.X * MovementSpeed * DeltaTime;
		Pos.Y += PlayerCharInput.MovementInput.Y * MovementSpeed * DeltaTime;
		SetActorLocation(Pos);
	}

	APlayerController* playerController = Cast<APlayerController>(GetController());
	float MouseX, MouseY;
	if (playerController && playerController->GetMousePosition(MouseX, MouseY)) {
		FVector2D CharOnScreenPosition;
		UGameplayStatics::ProjectWorldToScreen(playerController, GetActorLocation(), CharOnScreenPosition);
		float X = MouseX - CharOnScreenPosition.X;
		float Y = MouseY - CharOnScreenPosition.Y;
		FVector2D MouseDirection(X, Y);
		//UE_LOG(LogTemp, Warning, TEXT("Mouse X,Y: %f,%f -- Char X,Y %f,%f"), MouseX, MouseY, CharOnScreenPosition.X, CharOnScreenPosition.Y);
		MouseDirection = MouseDirection.GetSafeNormal();
		if (!MouseDirection.IsNearlyZero()) {
			float Angle = FMath::RadiansToDegrees(FMath::Atan2(MouseDirection.Y, MouseDirection.X));
			FRotator Rotation = PlayerDirection->GetComponentRotation();
			Rotation.Yaw = Angle + 90;
			PlayerDirection->SetWorldRotation(Rotation);
		}
	}
}

// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	PlayerInputComponent->BindAxis("MoveX", this, &APlayerPawn::MoveX);
	PlayerInputComponent->BindAxis("MoveY", this, &APlayerPawn::MoveY);
	PlayerInputComponent->BindAction("Shoot", EInputEvent::IE_Pressed, this, &APlayerPawn::ShootPressed);
	PlayerInputComponent->BindAction("Shoot", EInputEvent::IE_Released, this, &APlayerPawn::ShootReleased);
	PlayerInputComponent->BindAction("Reload", EInputEvent::IE_Pressed, this, &APlayerPawn::ReloadPressed);
}

void APlayerPawn::MoveX(float AxisValue) {
	//UE_LOG(LogTemp, Warning, TEXT("Axis Value: %f"), AxisValue);
	PlayerCharInput.MoveX(AxisValue);
}
void APlayerPawn::MoveY(float AxisValue) {
	PlayerCharInput.MoveY(AxisValue);
}

void APlayerPawn::ShootPressed() {
	UE_LOG(LogTemp, Warning, TEXT("Shoot Pressed"));
	PlayerCharInput.SetShooting(true);
}
void APlayerPawn::ShootReleased() {
	UE_LOG(LogTemp, Warning, TEXT("Shoot Released"));
	PlayerCharInput.SetShooting(false);
}
void APlayerPawn::ReloadPressed() {
	UE_LOG(LogTemp, Warning, TEXT("Reload Pressed"));
	Weapon->Reload();
}




void APlayerPawn::HitByZombie(float Damage) {
	HealthPoints -= Damage;
	/*APlayerController* playerController = Cast<APlayerController>(GetController());
	if (playerController) {
		playerController->ClientPlayCameraShake(USomeCameraShake::StaticClass(), 100);
	}*/
	if (HealthPoints <= 0) {
		Destroy();
	}
}


void FPlayerCharInput::Sanitize()
{
	MovementInput = RawMovementInput.ClampAxes(-1.0f, 1.0f);
	MovementInput = MovementInput.GetSafeNormal();
	RawMovementInput.Set(0.0f, 0.0f);
}

void FPlayerCharInput::MoveX(float AxisValue)
{
	RawMovementInput.X += AxisValue;
}

void FPlayerCharInput::MoveY(float AxisValue)
{
	RawMovementInput.Y += AxisValue;
}

void FPlayerCharInput::SetShooting(bool bPressed) {
	Shooting = bPressed;
}