// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "engine.h"
#include "GameFramework/Actor.h"
#include "ZombieSpawner.generated.h"

UCLASS()
class ZOMBIEDEFENSE_API AZombieSpawner : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AZombieSpawner();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;


	//The avarage cooldown between two zombie spawns.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ZombieSpawner", meta = (ClampMin = "0.0"))
	float AverageSpawnCooldown;

	//The zombie type that should be spawned by this spawner.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ZombieSpawner")
	TSubclassOf<AActor> ZombieToSpawn;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	//The box to determine where the zombies should spawn.
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "ZombieSpawner")
	UBoxComponent* SpawnBox;

private:

	//Handle to manage the zombie spawn timer
	FTimerHandle ZombieSpawnTimerHandle;

	void SpawnZombie();
};
