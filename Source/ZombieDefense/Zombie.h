// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"
#include "GameFramework/Pawn.h"
#include "Zombie.generated.h"

UCLASS()
class ZOMBIEDEFENSE_API AZombie : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AZombie();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	//Movement Speed
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Zombie", meta = (ClampMin = "0.0"))
		float MovementSpeed = 30;
	//Health Points
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (ClampMin = "0.0"))
		float HealthPoints = 100;
	//Damage the zombie deals per attack
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (ClampMin = "0.0"))
		float Damage = 40;

	//The cooldown between two attacks
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (ClampMin = "0.0"))
		float Cooldown = 0.5;

	class APlayerPawn* Target;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	
	//arrow to determine facing
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (AllowPrivateAccess = "true"))
	UArrowComponent* ZombieDirection;

	//Sprite for the Zombie
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (AllowPrivateAccess = "true"))
	class UPaperSpriteComponent* ZombieSprite;

	void HitByProjectile(class AProjectile* Projectile);


	//Width of the bounding box used for collision detection
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (ClampMin = "0.0"))
		float BoundingBoxWidth = 10;
	//Height of the bounding box used for collision detection
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Zombie", meta = (ClampMin = "0.0"))
		float BoundingBoxHeight = 100;

	void AttackTarget();

private:
	float CooldownTime;
};
