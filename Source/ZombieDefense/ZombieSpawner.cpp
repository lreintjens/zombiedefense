// Fill out your copyright notice in the Description page of Project Settings.


#include "ZombieSpawner.h"

// Sets default values
AZombieSpawner::AZombieSpawner()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = SpawnBox = CreateDefaultSubobject<UBoxComponent>(TEXT("SpawnBox"));
}

// Called when the game starts or when spawned
void AZombieSpawner::BeginPlay()
{
	Super::BeginPlay();

	if (UWorld* World = GetWorld()) {
		float ZombieSpawnTime = AverageSpawnCooldown;
		ZombieSpawnTime = FMath::RandRange(0.7f, 1.3f) * ZombieSpawnTime;
		World->GetTimerManager().SetTimer(ZombieSpawnTimerHandle, this, &AZombieSpawner::SpawnZombie, ZombieSpawnTime, false);
	}
}

// Called every frame
void AZombieSpawner::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

}

void AZombieSpawner::SpawnZombie() {

	if (UWorld* World = GetWorld()) {
		FVector SpawnLocation = FMath::RandPointInBox(SpawnBox->Bounds.GetBox());
		SpawnLocation.Z = 10.0f;
		AActor* SpawnedActor = World->SpawnActor(ZombieToSpawn);
		SpawnedActor->SetActorLocation(SpawnLocation);

		float ZombieSpawnTime = AverageSpawnCooldown;
		ZombieSpawnTime = FMath::RandRange(0.7f, 1.3f) * ZombieSpawnTime;
		World->GetTimerManager().SetTimer(ZombieSpawnTimerHandle, this, &AZombieSpawner::SpawnZombie, ZombieSpawnTime, false);
	}
}

