// Fill out your copyright notice in the Description page of Project Settings.


#include "Zombie.h"
#include "PaperSpriteComponent.h"
#include "Projectile.h"
#include "PlayerPawn.h"

// Sets default values
AZombie::AZombie()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	ZombieDirection = CreateDefaultSubobject<UArrowComponent>(TEXT("ZombieDirection"));
	ZombieDirection->AttachTo(RootComponent);

	ZombieSprite = CreateDefaultSubobject<UPaperSpriteComponent>(TEXT("ZombieSprite"));
	ZombieSprite->AttachTo(ZombieDirection);
}

// Called when the game starts or when spawned
void AZombie::BeginPlay()
{
	Super::BeginPlay();
	
	CooldownTime = 0;
}

// Called every frame
void AZombie::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (!Target) {
		UWorld* World = GetWorld();
		if (World) {
			TArray<AActor*> Players;
			UGameplayStatics::GetAllActorsOfClass(World, APlayerPawn::StaticClass(), Players);

			int Closest = -1;
			float Distance = -1;
			for (int i = 0; i < Players.Num(); i++) {
				float Dist = this->GetDistanceTo(Players[i]);
				if (Dist > Distance) {
					Distance = Dist;
					Closest = i;
				}
			}
			if (Closest != -1) {
				Target = Cast<APlayerPawn>(Players[Closest]);
			}
		}
	}
	else {
		FVector Loc = GetActorLocation();
		FVector TargetLocation = Target->GetActorLocation();
		FVector2D TargetVector(TargetLocation.X - Loc.X, TargetLocation.Y - Loc.Y);
		TargetVector = TargetVector.GetSafeNormal();
		if (!TargetVector.IsNearlyZero()) {
			float Angle = FMath::RadiansToDegrees(FMath::Atan2(TargetVector.Y, TargetVector.X));
			FRotator Rotation = ZombieDirection->GetComponentRotation();
			Rotation.Yaw = Angle /*+ 90*/;
			ZombieDirection->SetWorldRotation(Rotation);
		}

		Loc += (DeltaTime * MovementSpeed) * GetTransform().GetUnitAxis(EAxis::X);
		SetActorLocation(Loc);

		if (GetDistanceTo(Target) < 20) {
			AttackTarget();
		}
	}
}

// Called to bind functionality to input
void AZombie::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

}

void AZombie::HitByProjectile(AProjectile* Projectile) {
	HealthPoints -= Projectile->Damage;
	UE_LOG(LogTemp, Warning, TEXT("Zombie hit by projectile. New HP: %f"), HealthPoints);
	if (HealthPoints <= 0) {
		Destroy();
	}
}

void AZombie::AttackTarget() {
	if (UWorld* World = GetWorld()) {
		float CurrentTime = World->GetTimeSeconds();
		if (CooldownTime + Cooldown < CurrentTime) {
			CooldownTime = CurrentTime;
			Target->HitByZombie(Damage);
		}
	}
}